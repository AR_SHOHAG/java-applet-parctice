import java.awt.*;
import java.applet.*;

class Project extends Applet
{
	public void paint(Graphics g)
	{
		g.fillArc(100,100,86,86,0,270);
		g.fillArc(186,100,86,86,180,-270);
		g.drawOval(150,150,15,15);
		g.fillOval(154,154,7,7);
		g.drawOval(205,150,15,15);
		g.fillOval(209,154,7,7);
		g.drawLine(186,158,186,180);
		g.fillArc(143,143,86,86,180,180);
		g.fillArc(160,150,50,60,180,180);
	}
}